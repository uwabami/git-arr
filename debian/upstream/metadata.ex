# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/git-arr/issues
# Bug-Submit: https://github.com/<user>/git-arr/issues/new
# Changelog: https://github.com/<user>/git-arr/blob/master/CHANGES
# Documentation: https://github.com/<user>/git-arr/wiki
# Repository-Browse: https://github.com/<user>/git-arr
# Repository: https://github.com/<user>/git-arr.git
